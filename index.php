<?php
// index.php

// Database connection parameters.
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'todolist');
define('DB_HOST', '127.0.0.1');
define('DB_PORT', '3307');

// Establish a database connection using PDO.
try {
    $pdo = new PDO("mysql:host=" . DB_HOST . ";port=" . DB_PORT . ";dbname=" . DB_NAME, DB_USER, DB_PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die("Database connection failed: " . $e->getMessage());
}

// Handle actions (new, delete, done).
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $action = isset($_POST['action']) ? $_POST['action'] : null;

    switch ($action) {
        case 'new':
            if (isset($_POST['title'])) {
                $title = $_POST['title'];
                $stmt = $pdo->prepare("INSERT INTO todo (title) VALUES (:title)");
                $stmt->bindParam(':title', $title);
                $stmt->execute();

                // Fetch the latest task after adding.
                $stmt = $pdo->prepare("SELECT * FROM todo ORDER BY created_at DESC LIMIT 1");
                $stmt->execute();
                $latestTask = $stmt->fetch(PDO::FETCH_ASSOC);
            }
            break;
        case 'delete':
            if (isset($_POST['id'])) {
                $id = $_POST['id'];
                $stmt = $pdo->prepare("DELETE FROM todo WHERE id = :id");
                $stmt->bindParam(':id', $id);
                $stmt->execute();
            }
            break;
        case 'done':
            if (isset($_POST['id'])) {
                $id = $_POST['id'];
                $stmt = $pdo->prepare("UPDATE todo SET done = 1 - done WHERE id = :id");
                $stmt->bindParam(':id', $id);
                $stmt->execute();
            }
            break;
    }
}

// Read tasks from the 'todo' table.
$stmt = $pdo->prepare("SELECT * FROM todo ORDER BY created_at DESC");
$stmt->execute();
$taches = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Todo List</title>
    <!-- Bootstrap CDN -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Todo List</a>
</nav>

<div class="container mt-4">
    <form action="index.php" method="post">
        <div class="form-group">
            <label for="title">New Task:</label>
            <input type="text" class="form-control" id="title" name="title" required>
        </div>
        <button type="submit" class="btn btn-primary" name="action" value="new">Add Task</button>
    </form>

    <!-- Display the latest added task immediately -->
    <!-- <?php if (isset($latestTask)) : ?>
        <div class="alert alert-success mt-4" role="alert">
            Task added: <?= $latestTask['title'] ?>
        </div>
    <?php endif; ?> -->

    <ul class="list-group mt-4">
        <?php foreach ($taches as $tache) : ?>
            <li class="list-group-item <?= $tache['done'] ? 'list-group-item-success' : 'list-group-item-warning' ?>">
                <?= $tache['title'] ?>
                <form action="index.php" method="post" class="float-right">
                    <input type="hidden" name="id" value="<?= $tache['id'] ?>">
                    <button type="submit" class="btn btn-success btn-sm" name="action" value="done">Done</button>
                    <button type="submit" class="btn btn-danger btn-sm" name="action" value="delete">Delete</button>
                </form>
            </li>
        <?php endforeach; ?>
    </ul>
</div>

<!-- Bootstrap JS and Popper.js CDN -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

</body>
</html>
